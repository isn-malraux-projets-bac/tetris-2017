from tkinter import *
from time import sleep

#-------------------------------------------------------------------------------
def grille():
    """ Création du tableau de canevas 20 lignes 10 colonnes et de sa matrice + Définition d'une pièce et de ses rotations"""
    global tab_can,tab_grille,tab_couleur,coord_courante,piece

    tab_can=[]
    tab_grille=[]
    tab_couleur=['grey','blue']
    for i in range(20):
        tab_can.append([[]]*10)
        tab_grille.append([[]]*10)

    grille_jeu = Frame(fen)
    for ligne in range(20):
        for colonne in range(10):
            couleur = tab_couleur[0]
            tab_can[ligne][colonne] = Canvas(grille_jeu,bg=couleur,height=20,
                                           width=20,borderwidth=1,relief=RIDGE)
            tab_can[ligne][colonne].grid(row=ligne,column=colonne)
            tab_grille[ligne][colonne] = 0
    grille_jeu.grid(row=0,column=0)

    #piece =[[(0,1,0,0),(0,1,0,0),(0,1,0,0),(0,1,0,0)],
            #[(1,1,1,1),(0,0,0,0),(0,0,0,0),(0,0,0,0)]]
    piece = [[(0,1,1,0),(0,0,1,1),(0,0,0,0),(0,0,0,0)],
            [(0,1,0,0),(0,1,1,0),(0,0,1,0),(0,0,0,0)]]

    fen.bind('<Right>',droite)
    fen.bind('<Left>',gauche)
    fen.bind('<Up>',rotation)
    update_grille()
    new_piece()

#-------------------------------------------------------------------------------
def new_piece():
    """ C'est par le biais de coord_courante que l'on va se dÃƒÂ©placer
        dans la matrice soit horizontalement (gauche:dx=-1,droite:dx=1)
        soit verticalement (descente:dy=1).Sens reprÃƒÂ©sente la position de la
        piÃƒÂ¨ce dans le tableau contenant la piÃƒÂ¨ce courante."""
    global coord_courante,sens

    coord_courante = [0,3]
    sens = 0
    sleep(1)
    if not verif_deplacement(0,0,0):
        affiche_piece(sens)
        print ("Vous avez perdu, dommage réessayez !!!")
        fen.destroy()
    else:
        vitesse()

#-------------------------------------------------------------------------------
def vitesse():
    """On efface la piÃƒÂ¨ce et on teste si elle peut descendre toutes les 250ms.
    Dans le cas contraire on la rÃƒÂ©inscrit ÃƒÂ  sa position courante et on vÃƒÂ©rifie
    si une ou plusieurs lignes se sont formÃƒÂ©es."""
    efface_piece()
    if verif_deplacement(1,0,0):
        coord_courante[0] += 1
        affiche_piece(sens)
        fen.after(200,vitesse)
    else :
        affiche_piece(sens)
        verif_ligne()
        new_piece()

#-------------------------------------------------------------------------------
def gauche(event):
    """On efface la piÃƒÂ¨ce et on vÃƒÂ©rifie si elle peut se dÃƒÂ©placer vers la gauche.
    Dans le cas contraire on la rÃƒÂ©inscrit ÃƒÂ  sa position courante."""
    efface_piece()
    if verif_deplacement(0,-1,0):
        coord_courante[1] -=1
        affiche_piece(sens)
    else:
        affiche_piece(sens)

#-------------------------------------------------------------------------------
def droite(event):
    """On efface la piÃƒÂ¨ce et on vÃƒÂ©rifie si elle peut se dÃƒÂ©placer vers la droite.
    Dans le cas contraire on la rÃƒÂ©inscrit ÃƒÂ  sa position courante."""
    efface_piece()
    if verif_deplacement(0,1,0):
        coord_courante[1] += 1
        affiche_piece(sens)
    else:
        affiche_piece(sens)

#-------------------------------------------------------------------------------
def rotation(event):
    """On efface la piÃƒÂ¨ce et on vÃƒÂ©rifie si on peut la faire tourner.
    Dans le cas contraire on la rÃƒÂ©inscrit ÃƒÂ  sa position courante."""
    global sens

    efface_piece()
    if verif_deplacement(0,0,1):
        sens += 1
        if sens == 2:
            sens = 0
        affiche_piece(sens)
    else:
        affiche_piece(sens)

#-------------------------------------------------------------------------------
def efface_piece():
    """ Avant n'importe lequel des dÃƒÂ©placements on commence par effacer la piÃƒÂ¨ce
        de la matrice. Pour ceci on se place dans la matrice a la coord_courante,
        et on boucle en ligne et en colonne dans la piece.(coord_courante
        reprÃƒÂ©sente le coin supÃƒÂ©rieur gauche de la piÃƒÂ¨ce).Si la valeur de la
        piÃƒÂ¨ce dans une colonne donnÃƒÂ©e vaut zÃƒÂ©ro on passe ÃƒÂ  la colonne suivante
        pour ne pas effacer les blocs prÃƒÂ©existants.
        Dans le cas contraire son emplacement dans la matrice est mis a zÃƒÂ©ro."""
    for i in range(4):
        for j in range(4):
            if piece[sens][i][j] == 0:
                continue
            tab_grille[coord_courante[0]+i][coord_courante[1]+j] = 0

#-------------------------------------------------------------------------------
def affiche_piece(sens):
    """MÃƒÂªme principe que efface_Piece(), cependant ici on passe les coordonnÃƒÂ©es
       dans la matrice ÃƒÂ  la valeur de la piÃƒÂ¨ce."""
    for i in range(4):
        for j in range(4):
            if piece[sens][i][j] == 0:
                continue
            tab_grille[coord_courante[0]+i][coord_courante[1]+j] =\
                                                               piece[sens][i][j]
    update_grille()

#-------------------------------------------------------------------------------
def verif_deplacement(dy,dx,pivot):
    """La piÃƒÂ¨ce ÃƒÂ  ÃƒÂ©tÃƒÂ© effacÃƒÂ©e, on cherche maintenant ÃƒÂ  savoir si on peut la
       rÃƒÂ©inscrire vers la droite, vers la  gauche, vers le bas ou la faire
       tourner.Prenons l'exemple d'une translation vers la droite.
       On cherche d'abord ÃƒÂ  savoir si la piÃƒÂ¨ce ne sera pas en dehors de la
       matrice.Pour cela on se place ÃƒÂ  la coord_courante dans la matrice + le
       dÃƒÂ©calage(dx=1),et on boucle en ligne et en colonne dans la piÃƒÂ¨ce.
       Pour une ligne donnÃƒÂ©e,lorsque la coord_courante+dx+incrÃƒÂ©mentation en
       colonne dans la piÃƒÂ¨ce dÃƒÂ©passent la limite de la matrice en largeur, on
       vÃƒÂ©rifie la valeur de la colonne correspondante ÃƒÂ  cette incrÃƒÂ©mentation
       dans la piÃƒÂ¨ce.Si elle vaut zÃƒÂ©ro on poursuit l'incrÃƒÂ©mentation, dans le cas
       contraire on renvoie un dÃƒÂ©placement non possible ÃƒÂ  la mÃƒÂ©thode appelante.
       Si la piÃƒÂ¨ce ne "dÃƒÂ©borde" pas, on multiplie la valeur de la coordonnÃƒÂ©e de
       la piÃƒÂ¨ce par celles de la matrice ÃƒÂ  la coordonnÃƒÂ©e correspondante d'aprÃƒÂ¨s
       la incrÃƒÂ©mentations effectuÃƒÂ©es.Si le produit est non nul, un bloc est dÃƒÂ©jÃƒÂ 
       prÃƒÂ©sent donc on renvoie un dÃƒÂ©placement non possible."""
    rotation = sens + pivot
    if rotation == 2 :
        rotation = 0
    for i in range(4):
        for j in range(4):
            if coord_courante[1]+(dx+j) > 9 or coord_courante[1]+(dx+j) < 0 or\
               coord_courante[0]+(dy+i) > 19:
                if piece[rotation][i][j] != 0:
                    return False
                else:
                    continue
            if (piece[rotation][i][j] * tab_grille[coord_courante[0]+(dy+i)]\
                                              [coord_courante[1]+(j+dx)]) != 0:
                return False
    return True

#-------------------------------------------------------------------------------
def verif_ligne():
    """AppelÃƒÂ©e aprÃƒÂ¨s chaque pose de piÃƒÂ¨ce, cette mÃƒÂ©thode vÃƒÂ©rifie par rÃƒÂ©cursivitÃƒÂ©
    si au moins une ligne ÃƒÂ  ÃƒÂ©tÃƒÂ© construite.On se dÃƒÂ©place ligne par ligne de par
    le bas dans la matrice.Par dÃƒÂ©faut la ligne est considÃƒÂ©rÃƒÂ©e complÃƒÂ¨te.Pour
    chaque ligne on vÃƒÂ©rifie chaque colonnes. Si la valeur de la matrice ÃƒÂ  cette
    coordonnÃƒÂ©e vaut zÃƒÂ©ro la ligne n'est pas complÃƒÂ¨te et on passe ÃƒÂ  la ligne
    suivante. Lorsqu'une ligne est complÃƒÂ¨te on la supprime, on rajoute une ligne
    de "zÃƒÂ©ro" en premier indice et on refait une vÃƒÂ©rification de par le bas."""
    for ligne in range(19,-1,-1):
        ligne_complete = True
        for colonne in range(10):
            if tab_grille[ligne][colonne] == 0:
                ligne_complete = False

        if ligne_complete:
            del tab_grille[ligne]
            tab_grille[0:0] = [[0,0,0,0,0,0,0,0,0,0]]
            update_grille()
            verif_ligne()

#-------------------------------------------------------------------------------
def update_grille():
    """Mise a jour des canevas en fonction des couleurs rÃƒÂ©fÃƒÂ©rencÃƒÂ©es par les
    valeurs de la matrice."""
    for ligne in range(20):
        for colonne in range(10):
            couleur = tab_couleur[tab_grille[ligne][colonne]]
            tab_can[ligne][colonne].configure(bg=couleur)

    fen.update()


fen = Tk()
fen.title("TETRIS")
grille()
fen.mainloop()
